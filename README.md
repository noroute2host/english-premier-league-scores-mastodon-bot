# Bot Scoreboard Football FIFA World Cup 2022 para Mastodon

Mastodon bot for English Premier League Scoreboard. Running live in https://mastodon.world/@premierleaguescores

## Tabla de contenidos

*   [About the project](#about-the-project)
*   [Getting Started](#getting-started)
    *   [Prerequisites](#prerequisites)
    *   [Installation](#installation)
    *   [Usage](#usage)
*   [Build with](#build-with)
*   [Contributing](#contributing)
*   [License](#licencia)

## About the project

This project implements a Mastodon bot for tooting the FIFA World Cup 2022 Scoreboard.

It's forked from FIFA World Cup 2022 Scores Mastodon bot: https://gitlab.com/noroute2host/fifa-world-cup-scores-mastodon-bot

Just now, this bot is running live in  https://mastodon.world/@premierleaguescores account at https://mastodon.world instance.

![fifaworldcupscores bot en tusky](imgs/premierleaguescores_tusky.jpg)

This code uses ESPN API for getting data: https://site.api.espn.com/apis/site/v2/sports/soccer/eng.1/scoreboard
 
You can use this project to learn how to make your own Mastodon bot. In the other hand, you can collaborate and improve this bot. Or you can transform it for using with other sports.

## Getting Started

### Prerequisites

* Python 3.8+ (It should work with 3.6+ too)
* Additional libraries: (pip freeze example, but it should work with similar versions)
    * blurhash==1.1.4
    * certifi==2022.9.24
    * charset-normalizer==2.1.1
    * configparser==5.3.0
    * decorator==5.1.1
    * idna==3.4
    * Mastodon.py==1.6.3
    * python-dateutil==2.8.2
    * python-magic==0.4.27
    * pytz==2022.6
    * requests==2.28.1
    * six==1.16.0
    * urllib3==1.26.12

### Installation

#### Building a new virtualenv

Python virtualenv is recommended to install and build your emvironmment to avoid conflicts with other python apps.

#### Clone the repo

You can use git to clone this repo in your computer.

```bash
git clone https://gitlab.com/noroute2host/english-premier-league-scores-mastodon-bot.git
```

### Install Prerequisites

Install the [prerequisites](#prerequisites) with pip one by one or using requeriments.txt from repo.

#### Configuration file

This software needs a config file to works. You need to generate a config.conf file with the following format.

```ini
[API]
API_URL_SCB = https://site.api.espn.com/apis/site/v2/sports/soccer/eng.1/scoreboard

[MASTODON]
ACCESS_TOKEN = SUPERSECRETTOKEN
API_BASE_URL = https://mastodon.world/
TOOTEAR = True
MAX_TAM_TOOT = 450

[PUBLISH_CONFIG]
GAMEDAY_PUBLISH_HOURS = 0,1,4,7,10,11,12,13,14,15,16,17,18,19,20,21,22,23
NO_GAMEDAY_PUBLISH_HOURS = 0,4,8,12,16,20
```

There is a config file example in this repo.

### Usage

#### Simple run

```bash
python main.py
```

#### Scheduling with cron.

This bot can be automated with crond or any other automation software. Try it yourself!

## Build with

* [Python](https://www.python.org/)
* [ESPN API](https://site.api.espn.com/apis/site/v2/sports/soccer/eng.1/scoreboard)

## Contributing

Contributing with source code 💻

- 🍴 Fork the project [here](https://gitlab.com/noroute2host/english-premier-league-scores-mastodon-bot/-/forks/new)
- 🔨 Modify the code
- 😊 Add yourself has contributor
- 🔧 Do a pull request [here](https://gitlab.com/noroute2host/english-premier-league-scores-mastodon-bot/-/merge_requests)

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are greatly appreciated.

Please, open an issue firts to discuss the change.

Openning an issue 😊

- 😯 Open an issue [here](https://gitlab.com/noroute2host/english-premier-league-scores-mastodon-bot/-/issues)

## Donate

You can send **Bitcoin** donations to the following address to support FIFA World Cup Scoreboard Mastodon Bot:

![fifaworldcupscores donate](imgs/btc-donations.png)

1KsBBAPidWCpf8rVSLjyjoRNg3b5jdsJ1q

## License

Distributed under the **GNU General Public License v3.0**. See LICENSE.txt for more information.

